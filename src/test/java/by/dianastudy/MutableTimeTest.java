package by.dianastudy;

import org.junit.Assert;
import org.junit.Test;

public class MutableTimeTest {
    @Test
    public void tryToCreateValidTime1() {
        int expectedHours = 0;
        int expectedMinutes = 0;

        TimeCreateTry timeCreateTry = new TimeCreateTry(expectedHours, expectedMinutes);

        Assert.assertFalse(timeCreateTry.isExceptionThrown());
        Time time = timeCreateTry.getTime();
        Assert.assertNotNull(time);
        Assert.assertEquals(expectedHours, time.getHours());
        Assert.assertEquals(expectedMinutes, time.getMinutes());
    }

    @Test
    public void tryToCreateValidTime2() {
        int expectedHours = 23;
        int expectedMinutes = 0;

        TimeCreateTry timeCreateTry = new TimeCreateTry(expectedHours, expectedMinutes);

        Assert.assertFalse(timeCreateTry.isExceptionThrown());
        Time time = timeCreateTry.getTime();
        Assert.assertNotNull(time);
        Assert.assertEquals(expectedHours, time.getHours());
        Assert.assertEquals(expectedMinutes, time.getMinutes());
    }

    @Test
    public void tryToCreateValidTime3() {
        int expectedHours = 0;
        int expectedMinutes = 59;

        TimeCreateTry timeCreateTry = new TimeCreateTry(expectedHours, expectedMinutes);

        Assert.assertFalse(timeCreateTry.isExceptionThrown());
        Time time = timeCreateTry.getTime();
        Assert.assertNotNull(time);
        Assert.assertEquals(expectedHours, time.getHours());
        Assert.assertEquals(expectedMinutes, time.getMinutes());
    }

    @Test
    public void tryToCreateValidTime4() {
        int expectedHours = 23;
        int expectedMinutes = 59;

        TimeCreateTry timeCreateTry = new TimeCreateTry(expectedHours, expectedMinutes);

        Assert.assertFalse(timeCreateTry.isExceptionThrown());
        Time time = timeCreateTry.getTime();
        Assert.assertNotNull(time);
        Assert.assertEquals(expectedHours, time.getHours());
        Assert.assertEquals(expectedMinutes, time.getMinutes());
    }

    @Test
    public void tryToCreateInvalidTime1() {
        int expectedHours = -11;
        int expectedMinutes = 0;

        TimeCreateTry timeCreateTry = new TimeCreateTry(expectedHours, expectedMinutes);
        Assert.assertTrue(timeCreateTry.isExceptionThrown());
    }

    @Test
    public void tryToCreateInvalidTime2() {
        int expectedHours = -11;
        int expectedMinutes = 69;

        TimeCreateTry timeCreateTry = new TimeCreateTry(expectedHours, expectedMinutes);
        Assert.assertTrue(timeCreateTry.isExceptionThrown());
    }

    @Test
    public void tryToCreateInvalidTime3() {
        int expectedHours = 1;
        int expectedMinutes = 69;

        TimeCreateTry timeCreateTry = new TimeCreateTry(expectedHours, expectedMinutes);
        Assert.assertTrue(timeCreateTry.isExceptionThrown());
    }
}


class TimeCreateTry {
    private Time time;
    private Exception exception;

    TimeCreateTry(int testHours, int testMinutes) {
        try {
            this.time = new MutableTime(testHours, testMinutes);
        } catch (IllegalArgumentException e) {
            exception = e;
        }
    }

    Time getTime() {
        return time;
    }

    boolean isExceptionThrown() {
        return exception != null;
    }
}
