package by.dianastudy;

public interface Time {
    int add(Time time);
    int addMinutes(int minutes);
    int addHours(int hours);
    int getHours();
    int getMinutes();
}

