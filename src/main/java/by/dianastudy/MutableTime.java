package by.dianastudy;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;


public class MutableTime implements by.dianastudy.Time{
    private int hours;
    private int minutes;

    public MutableTime(int hours, int minutes) {
        if (hours < 0 || hours >= 24 || minutes < 0 || minutes >= 60) {
            throw new IllegalArgumentException(String.format("Time %s:%s is invalid.", hours, minutes));
        }

        this.hours = hours;
        this.minutes = minutes;
    }

    public int getHours() {
        return hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public int add(Time time) {
        throw new NotImplementedException();
    }

    public int addMinutes(int minutes) {
        throw new NotImplementedException();
    }

    public int addHours(int hours) {
        throw new NotImplementedException();
    }
}
